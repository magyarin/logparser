import datetime
import sys

class LogLine:
  def __init__(self, requestType, requestDate):
    self.requestType = requestType
    self.requestDate = requestDate
    self.responseDate = None
  def __str__(self):
      return "requestType: {0}, requestDate: {1}, responseDate: {2}".format(self.requestType, self.requestDate,self.responseDate)


if len(sys.argv) != 2:
    sys.exit(-1)

messageLogFile = open(sys.argv[1], "r", 100, "cp850")
lines = messageLogFile.readlines()
print(lines[0])

print(lines[0].split("|"))

#date_time_str = '2018-06-29 08:15:27.243860'
#date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%d %H:%M:%S.%f')

LogLineMap = {}

for line in lines:
    splittedLine = line.split("|")
    date = datetime.datetime.strptime("2020." + splittedLine[0], '%Y.%m.%d %H:%M:%S.%f')
    #print(splittedLine)
    try:
        requestType = splittedLine[1]
        correlationId = splittedLine[3].split("-")[2]

        if correlationId not in LogLineMap:
            LogLineMap[correlationId] = LogLine(requestType, date)
        else:
            LogLineMap.get(correlationId).responseDate = date
    except IndexError:
        correlationId = 0

differenceDict = {}

for key in LogLineMap:
    #print(logLine)
    if LogLineMap[key].responseDate is not None:
        differenceInSeconds = (LogLineMap[key].responseDate - LogLineMap[key].requestDate).total_seconds()
        if differenceInSeconds > 1:
            #print(str(LogLineMap[key]).__add__(", correlationId: " + key))
            #print(differenceInSeconds)
            differenceDict[(str(LogLineMap[key]).__add__(", correlationId: " + key))] = differenceInSeconds
    #print(str(logLine.date) + " corr: " + str(logLine.correlationId))

sortedLogLines = {k: v for k, v in reversed(sorted(differenceDict.items(), key=lambda item: item[1]))}

for key in sortedLogLines:
    print("roundtrip time(s): " + str(sortedLogLines[key]).__add__(", " + key))
