import json
import sys
from collections import Counter


"""Extract nested values from a JSON tree."""
def id_generator(dict_var):
    for k, v in dict_var.items():
        if k == "errorCodes":
            yield v
        elif isinstance(v, dict):
            for id_val in id_generator(v):
                yield id_val

if len(sys.argv) != 2:
    sys.exit(-1)

messageLogFile = open(sys.argv[1], "r", 100, "cp850")
lines = messageLogFile.readlines()

errorCodes = []

for line in lines:
    try:
        firstValue = line.index("{")
        lastValue = len(line) - line[::-1].index("}")
        jsonString = line[firstValue:lastValue]

        for _ in id_generator(json.loads(jsonString)):
            errorCodes.append(_)
    except ValueError:
        pass
        #print(line) #notable to read



#print(errorCodes)
print(Counter(([i for j in errorCodes for i in j])))